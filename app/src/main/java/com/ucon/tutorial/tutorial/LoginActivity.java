package com.ucon.tutorial.tutorial;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button buttonSignIn = (Button) findViewById(R.id.buttonSignIn);
        buttonSignIn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonSignIn:
                //sebuah perintah
                Toast.makeText(getBaseContext(),"Halo !!",Toast.LENGTH_LONG).show();
                Intent bukaMainActivity = new Intent (this,MainActivity.class);
                startActivity(bukaMainActivity);
                break;
        }
    }
}
